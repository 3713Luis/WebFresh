import { Component } from '@angular/core';
// import { template } from '@angular/core/src/render3';

// @Component({
//     selector: 'helloWorld',
//     template: '<h1>Hello World</h1>',
//     styles:  ['h1 {background: #000; color: #fff;}']
// })

// @Component({
//     selector: 'helloWorld',
//     template: '<div class = "titulo"><h1>Hello World</h1><h2>Subtitulo</h2></div>',
//     styles:  ['.titulo {background: #000; color: #fff;}']
// })

@Component({
    selector: 'helloWorld', //nombre de etiqueta o componente
    templateUrl: './primer.Component.html', //archivo html
    styleUrls:  ['./primer.component.css'] //archivo de estilso
})
export class HelloWorld{
    title = 'Wlcome to Angular'; //variable que se mostrara como titulo
}
