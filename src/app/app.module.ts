import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HelloWorld } from './primerComponente/primer.Component';
import { MiNombreComponent } from './mi-nombre/mi-nombre.component';

@NgModule({
  declarations: [
    AppComponent,
    HelloWorld,
    MiNombreComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
